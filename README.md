# secrets

This is a compilation of _non-functional_ "secrets" one can use to test secret detection scanner functionality.

This repository does **not** contain any real, working, or valid secrets, passwords, tokens, keys, credentials, etc.

